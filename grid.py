import pygame
from math import floor
import random
import numpy as np

class Grid:
    def __init__(self,size, grid_density):
        self.WIDTH, self.HEIGHT = size
        self.fields_count_x, self.fields_count_y = grid_density
        
        self.field_width = self.WIDTH / self.fields_count_x
        self.field_height = self.HEIGHT / self.fields_count_y
        self.field_size = (self.field_width,self.field_height)

        self.FIELDS = pygame.sprite.Group()
        self.AMMOS = pygame.sprite.Group()
        self.FIELDS_LOOKUP = {}

        self.lava_fields_count_x = 3
        self.lava_fields_count_y = 2
        self.LAVA_WIDTH = self.lava_fields_count_x*self.field_width
        self.LAVA_HEIGHT = self.lava_fields_count_y*self.field_height
#        print("Lava WIDTH: {}, Lava HEIGHT: {}".format(self.LAVA_WIDTH,self.LAVA_HEIGHT))
        self.generateGrid()
        self.generateAmmo()

    def generateHeatMap(self, Q):
        self.FIELDS = pygame.sprite.Group()
        self.FIELDS_LOOKUP = {}
       
        max_val = np.max(Q);
        min_val = np.min(Q);
        max(Q.min(), Q.max(), key=abs)
        
        for i in range(self.fields_count_x):
            for k in range(self.fields_count_y):
                field_index = (i,k)
                state = self.getLavaState(field_index)
                cur_val = np.mean(Q[state])
                if cur_val > 0:
                    color_val = cur_val/max_val*255
                    color = (0,color_val,0)
                else:
                    color_val = abs(cur_val/min_val)*255
                    color = (color_val,0,0)
                print("Cur val {}".format(cur_val))
                print("Color {}".format(color))
                field = Field.getColorField(self.field_size,field_index,color);
                self.FIELDS.add(field)
                self.FIELDS_LOOKUP[i,k] = field

    def getLavaState(self, pos):
        i,k = pos
        is_left_lava = i < self.lava_fields_count_x
        is_right_lava = i >= (self.fields_count_x - self.lava_fields_count_x)
        is_top_lava = k < self.lava_fields_count_y
        is_bottom_lava = k >= (self.fields_count_y - self.lava_fields_count_y)

        if(is_left_lava):
            state = 0
        elif(is_right_lava):
            state = 1
        elif(is_top_lava):
            state = 2
        elif(is_bottom_lava):
            state = 3
        else:
#            margin = 2
#            is_left = i < self.lava_fields_count_x + margin
#            is_right = i >= (self.fields_count_x - self.lava_fields_count_x - #margin)
#            is_top = k < self.lava_fields_count_y + margin
#            is_bottom = k >= (self.fields_count_y - self.lava_fields_count_y - #margin)
            
            #Example 1            
            state = 4

            #Example 2
            #if(is_left):
             #   state = 4
            #elif(is_right):
            #    state = 5
            #elif(is_top):
            #    state = 6
            #elif(is_bottom):
            #    state = 7
            #else:
            #    state = 8
        return state

    def getLavaStateAbs(self,pos):
        x,y = pos
#        state = 1
        #@TODO
        left_dist = x - self.LAVA_WIDTH
#        print('X',x,'LAVA_WIDTH',self.LAVA_WIDTH)
        right_dist = self.WIDTH - self.LAVA_WIDTH - x
        up_dist = y - self.LAVA_HEIGHT
        down_dist = self.HEIGHT - self.LAVA_HEIGHT - y
#        state = [left_dist,right_dist,up_dist,down_dist]
        state = [left_dist,up_dist]
        return state

    def getAmmoState(self,pos):
        #@TODO if no ammos on ground this will fail hard
        ammo_x,ammo_y = self.AMMOS.sprites()[0].rect.center
        x,y = pos
        is_ammo_left = x > ammo_x + 4
        is_ammo_right = x < ammo_x - 4
        is_ammo_up = y > ammo_y + 4
        is_ammo_down = y < ammo_y - 4

        if(is_ammo_left):
            state = 0
        elif(is_ammo_right):
            state = 1
        elif(is_ammo_up):
            state = 2
        elif(is_ammo_down):
            state = 3
        
        return state

    def getAmmoStateAbs(self,pos):
        ammo_x,ammo_y = self.AMMOS.sprites()[0].rect.center
        x,y = pos

#        left_dist = max(0,x - ammo_x)
#        right_dist = max(0,ammo_x - x)
#        up_dist = max(0, ammo_y - y)
#        down_dist = max(0, y - ammo_y)
        horizontal_dist = x - ammo_x
        vertical_dist = ammo_y - y
        state = [horizontal_dist,vertical_dist]
        return state

    def generateGrid(self):
        for i in range(self.fields_count_x):
            for k in range(self.fields_count_y):
                field_index = (i,k)
                field = self.generateField(field_index)
                self.FIELDS.add(field)
                self.FIELDS_LOOKUP[i,k] = field

    def generateAmmo(self):
        rand_pos = self.getRandomGrassPosition()
        self.AMMOS.add(Ammo(rand_pos))

    def getRandomGrassPosition(self):
#        print("Generate x randrange({},{})".format(self.LAVA_WIDTH, self.WIDTH - self.LAVA_WIDTH))
        rand_x = random.randrange(self.LAVA_WIDTH, self.WIDTH - self.LAVA_WIDTH)
#        print("Generate y randrange({},{})".format(self.LAVA_HEIGHT, self.HEIGHT - self.LAVA_HEIGHT))
        rand_y = random.randrange(self.LAVA_HEIGHT, self.HEIGHT - self.LAVA_HEIGHT)
        rand_pos = (rand_x,rand_y)
#        print("Rand pos generated:",rand_pos)
        return rand_pos

    def generateField(self,pos):
        i,k = pos
        is_left_lava = i < self.lava_fields_count_x
        is_right_lava = i >= (self.fields_count_x - self.lava_fields_count_x)
        is_top_lava = k < self.lava_fields_count_y
        is_bottom_lava = k >= (self.fields_count_y - self.lava_fields_count_y)
        if(is_left_lava or is_right_lava or is_top_lava or is_bottom_lava):
            return Field.getLavaField(self.field_size,pos)
        else:
            if(random.randrange(0, 10) == 1):
                return Field.getIceField(self.field_size,pos)
            else:
                return Field.getGrassField(self.field_size,pos)
            

    def getFieldByPos(self,pos):
        i = floor(pos[0]/self.field_width)
        k = floor(pos[1]/self.field_height)
        if(i >= self.fields_count_x):
            i = self.fields_count_x -1
        if(k >= self.fields_count_y):
            k = self.fields_count_y - 1
        return self.FIELDS_LOOKUP[i,k]
        

class Field(pygame.sprite.Sprite):

    TYPE_GRASS = 1
    TYPE_ICE = 3
    TYPE_LAVA = 2

    def __init__(self, position, image, friction_mk, index, dps = 0):
        super().__init__()
        self.image = image
        self.rect = self.image.get_rect()
        self.rect.x, self.rect.y = position
        self.friction_mk = friction_mk
        self.dps = dps
        self.index = index
        self.type = None

    @staticmethod
    def getLavaField(size,index):
        w,h = size
        i,k = index
        pos = (i*w,k*h)
        image = pygame.image.load("assets/crawl-tiles/dc-dngn/floor/lava"+str(random.randrange(0,4))+".png")
        field = Field(pos,image,40,index, dps = 15)
        field.type = Field.TYPE_LAVA
        return field

    @staticmethod
    def getGrassField(size,index):
        w,h = size
        i,k = index
        pos = (i*w,k*h)
        image = pygame.image.load("assets/crawl-tiles/dc-dngn/floor/bog_green"+str(random.randrange(0,4))+".png")
        field = Field(pos,image,40,index)
        field.type = Field.TYPE_GRASS
        return field

    @staticmethod
    def getIceField(size,index):
        w,h = size
        i,k = index
        pos = (i*w,k*h)
        image = pygame.image.load("assets/crawl-tiles/dc-dngn/water/dngn_shoals_shallow_water"+str(random.randrange(1,5))+".png")
        field = Field(pos,image,0.5,index)
        field.type = Field.TYPE_GRASS
        return field
    
    @staticmethod
    def getColorField(size,index,color):
        w,h = size
        i,k = index
        pos = (i*w,k*h)        
        image = pygame.Surface([w,h])
        image.fill(color)
        field = Field(pos,image,0.5,index)
        field.type = 99999
        return field

class Ammo(pygame.sprite.Sprite):
    def __init__(self, position, mass = 5):
        super().__init__()
#        self.image = pygame.Surface([10,10])
        self.image = pygame.image.load("assets/crawl-tiles/item/weapon/double_sword.png")
#        self.image.fill((255,255,255))
        self.rect = self.image.get_rect()
        self.rect.x, self.rect.y = position
        self.mass = mass
