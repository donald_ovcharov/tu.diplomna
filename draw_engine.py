import pygame
import os, sys

class DrawEngine(object):
    WIDTH,HEIGHT = (960,512)

    def __init__(self,env_, name):
        #init game engine
        self.env = env_
        #position game window at center
        os.environ['SDL_VIDEO_CENTERED'] = '1'
        #init main surface
        self.screen = pygame.display.set_mode((self.WIDTH,self.HEIGHT))
        #set window title
        pygame.display.set_caption(name)

    def render(self):
        self.drawMap()
        self.drawUnits()
        self.drawBullets()
        self.drawStats()
        pygame.display.update()

    def drawMap(self):
        self.env.GRID.FIELDS.draw(self.screen)
        self.env.GRID.AMMOS.draw(self.screen)

    def drawUnits(self):
        self.env.UNITS.draw(self.screen)

    def drawBullets(self):
        for u in self.env.UNITS:
            u.BULLETS.draw(self.screen)   

    def drawStats(self):
        u1 = self.env.UNIT_1
        u2 = self.env.UNIT_2
        u1_stats = self.env.UNIT_1_STATS
        u2_stats = self.env.UNIT_2_STATS
        
        self.screen.blit(u1_stats.getHealthBar(),(u1.rect.left,u1.rect.top - 5))
        self.screen.blit(u2_stats.getHealthBar(),(u2.rect.left,u2.rect.top - 5))
        self.screen.blit(u1_stats.getLoadedBulletText(),(u1.rect.left,u1.rect.top - 20))
        self.screen.blit(u2_stats.getLoadedBulletText(),(u2.rect.left,u2.rect.top - 20))
        self.screen.blit(u1_stats.getCooldownBar(),(u1.rect.left,u1.rect.top))
        self.screen.blit(u2_stats.getCooldownBar(),(u2.rect.left,u2.rect.top))
