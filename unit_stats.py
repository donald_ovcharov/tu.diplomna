import pygame
import math

class UnitStats:
    def __init__(self, unit_):
        self.unit = unit_
        pygame.font.init()
        self.health_font = pygame.font.SysFont("comicsans", 20)
        self.bullet_font = self.health_font

    def getHealthBar(self):
        max_ = 100
        cur_ = round(self.unit.health)
        size = (40,5)
        color = (0,255,0)
        return self.getBar(max_,cur_,size,color)

    def getLoadedBulletText(self):
        text = str(self.unit.LOADED_BULLET.mass)+ " kg"
        color = (0, 0, 50)
        bullet_text = self.bullet_font.render(text, True, color)
        surface = pygame.Surface(self.bullet_font.size(text))
        surface.fill((255,255,255))
        surface.blit(bullet_text,(0,0))
        return surface

    def getCooldownBar(self):
        max_ = self.unit.LOADED_BULLET.COOLDOWN
        cur_ = self.unit.LOADED_BULLET.time
        size = (40,5)
        color = (255,255,255)
        return self.getBar(max_,cur_,size,color)

    def getBar(self,max_,cur_,size,color):

        if(cur_ > max_):
            cur_ = max_

        percent = cur_/max_

        width,height = size
        cur_width = round(width*percent)

        surface = pygame.Surface(size)
        bar = pygame.Rect((0,0),(cur_width,height))
        bar_color = color
        pygame.draw.rect(surface,bar_color,bar)
        return surface
    
