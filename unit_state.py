import grid
import numpy as np
from helpers import var_dump
from helpers import getDistance

class UnitState(object):
    REWARD_MAP = {
            grid.Field.TYPE_GRASS: 1,
#            grid.Field.TYPE_ICE: - 0,
            grid.Field.TYPE_LAVA: -1
        }

    def __init__(self,env_,unit_):
        self.env = env_
        self.unit = unit_
        self.unit_field = self.env.GRID.getFieldByPos(self.unit.rect.center)
        
    
    def hash(self):
        #1-5
#        print("Lava state = {}, Ammo state = {}".format(lava_state,ammo_state))
#        print(lava_state+ammo_state)
#        exit()
#        X = np.zeros(9)
#        X[lava_state]=1
#        X[5+ammo_state+1]=1
#        X = self.lava_state+self.ammo_state
#        X = self.ammo_state
        X = np.array(self.unit.rect.center)/1000 #max is 936,438
        X = np.append(X,self.unit.health/100)
        X = np.append(X,self.unit.hit_factor/30)
#        X = np.append(X,self.unit.acceleration)
#        X = np.append(X,self.unit.angle)
#        X = np.append(X,self.unit.velocity)
        X = np.append(X,self.unit.LOADED_BULLET.mass/100)
#        X = np.append(X,np.clip(self.unit.LOADED_BULLET.time,0,self.unit.LOADED_BULLET.COOLDOWN)*2.5)
        X = np.append(X,self.unit_field.dps/15)
        X = np.append(X,self.unit_field.friction_mk/40)
        bullets = list(self.unit.BULLETS.sprites())
        bullets.sort(key=lambda x: getDistance(x.rect.center,self.unit.rect.center))
        bullets = bullets[0:2]
        bullet_count = len(bullets)
        has_bullet = bullet_count > 0
        has_bullet2 = bullet_count > 1
        X = np.append(X, np.array(bullets[0].rect.center,dtype='float') if has_bullet else np.array([0,0]))
        X = np.append(X, bullets[0].angle/360 if has_bullet else 0)
        X = np.append(X, np.array(bullets[1].rect.center,dtype='float') if has_bullet2 else np.array([0,0]))
        X = np.append(X, bullets[1].angle/360 if has_bullet2 else 0)
        
#        max_unit_bullets_on_screen = 7
#        i = 0
#        for b in self.unit.BULLETS:
#            i += 1
#            if(i>max_unit_bullets_on_screen):
#                break
#            X = np.append(X,np.array(b.rect.center,dtype='float')/1000)
#        for i in np.arange(max_unit_bullets_on_screen - i):
#            X = np.append(X,[0,0])
        return X

    def reward(self):
        health_factor = np.clip(self.unit.health,0,100)/100 #max1
        dps_factor = 1-(self.unit_field.dps/15)#max1
        friction_factor = self.unit_field.friction_mk/40#max1
        ammos_factor = np.clip(self.unit.LOADED_BULLET.mass,0,100)/100#max1
        hit_factor = 1-np.clip(self.unit.hit_factor,0,30)/30#max1
        reward = health_factor*0 + dps_factor*0.7 + hit_factor*0.3 + ammos_factor*0
        return reward
