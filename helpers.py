from math import atan2, degrees, pi, sqrt

def getAngle(pos1,pos2):
    x1,y1 = pos1
    x2,y2 = pos2
    dx = x2 - x1
    dy = y2 - y1
    rads = atan2(-dy,dx)
    rads %= 2*pi
    degs = degrees(rads)
    return degs

def getDistance(pos1,pos2):
    x1,y1 = pos1
    x2,y2 = pos2
    return sqrt( (x2 - x1)**2 + (y2 - y1)**2 )

def bitfield(n,length = 4):
    bin_list = [1 if digit=='1' else 0 for digit in bin(n)[2:]]
    padding = length - len(bin_list)
    return [0]*padding + bin_list

def var_dump(val,label):
    print("============"+label+"==============")
    print(val)
