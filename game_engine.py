import pygame
import os, sys
import grid
import unit
import unit_stats
import unit_control
import unit_state
import random
import numpy as np
from helpers import var_dump

#@TODO 
# split render logic and game logic
# follow OpenAI frame
# for i_episode in range(20):
#    observation = env.reset()
#    for t in range(100):
#        env.render()
#        print(observation)
#        action = env.action_space.sample()
#        observation, reward, done, info = env.step(action)
#        if done:
#            print("Episode finished after {} timesteps".format(t+1))
#            break

class GameEngine(object):

    WIDTH,HEIGHT = (960,512)
    

    CONTROLS = []
    AI_CONTROL = None
    BOT_CONTROL = None
    CLOCK = pygame.time.Clock()
    FPS = 30
    MOMENT = 1/FPS

    def __init__(self, name):
        self.GRID = grid.Grid((960,512),(30,16))
        self.UNIT_1 = False
        self.UNIT_2 = False
        self.UNITS = pygame.sprite.Group()
        self.LEARNING_UNIT = False

    def dispatch(self, event):
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            sys.exit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            pass

        for uc in self.CONTROLS:
            uc.dispatch(event)

    def showTrainedMap(self, Q):
        self.GRID.generateHeatMap(Q);
        self.UNITS = pygame.sprite.Group()

    def start(self):
        HUMAN_UNIT_IMAGE = pygame.image.load("assets/images/mk3_liu_kang.gif")
        HUMAN_UNIT = unit.Unit(10, HUMAN_UNIT_IMAGE, self)

        AI_UNIT_IMAGE = pygame.image.load("assets/images/mk3_liu_kang2.gif")
        AI_UNIT = unit.Unit(10,AI_UNIT_IMAGE, self)

#        UC = unit_control.UnitControl(HUMAN_UNIT)

        self.setUnit1(HUMAN_UNIT)
        self.setUnit2(AI_UNIT)
#        self.addControl(UC)

    def restart(self):
        self.__init__('restarted')
        self.start()
#        var_dump("","GAME RESTARTED")

    def run(self):
        while not self.checkWin():
            self.step(pygame.event.get())
            self.render()
            self.CLOCK.tick(self.FPS)

    def step(self,actions):
        self.handleEvents(actions)
        self.processGame()
        self.handleBulletsCollide()
        self.handleAmmosCollide()
        return self.getStateReward()

    def getStateReward(self):
        unit_1_state = unit_state.UnitState(self,self.UNIT_1)
        unit_2_state = unit_state.UnitState(self,self.UNIT_2)
        state = np.append(unit_1_state.hash(),unit_2_state.hash())
        reward = unit_1_state.reward() - unit_2_state.reward()
        return state,reward

    def handleEvents(self,events):
        for event in events:
            self.dispatch(event)

    def processGame(self):
        for u in self.UNITS:
            u.field = self.GRID.getFieldByPos(u.rect.center)
            u.processFrame(self.MOMENT)
            for b in u.BULLETS:
                b.field = self.GRID.getFieldByPos(b.rect.center)
                b.processFrame(self.MOMENT)
                if(b.rect.x > self.WIDTH or b.rect.x < 0 or b.rect.y > self.HEIGHT or b.rect.y < 0):
                    b.kill()

    def handleBulletsCollide(self):
        unit2_hits = pygame.sprite.spritecollide(self.UNIT_2,self.UNIT_1.BULLETS,True)
        if(unit2_hits):
            self.UNIT_2.handleHits(unit2_hits)
        unit1_hits = pygame.sprite.spritecollide(self.UNIT_1,self.UNIT_2.BULLETS,True)
        if(unit1_hits):
            self.UNIT_1.handleHits(unit1_hits)

    def handleAmmosCollide(self):
        unit_1_ammos = pygame.sprite.spritecollide(self.UNIT_1,self.GRID.AMMOS, True)
        if(unit_1_ammos):
            self.UNIT_1.LOADED_BULLET.collect(unit_1_ammos)
            self.GRID.generateAmmo()
        unit_2_ammos = pygame.sprite.spritecollide(self.UNIT_2,self.GRID.AMMOS, True)
        if(unit_2_ammos):
            self.UNIT_2.LOADED_BULLET.collect(unit_2_ammos)
            self.GRID.generateAmmo()

    def checkWin(self):
        return self.UNIT_1.health <=0 or self.UNIT_2.health <=0


#ako se polzva 2 puti v UNITS 6e ima 3+ usera
    def setUnit1(self,u):
        self.UNIT_1 = u
        self.UNIT_1_STATS = unit_stats.UnitStats(self.UNIT_1)
        self.UNIT_1.setPosition(self.GRID.getRandomGrassPosition())
        self.BOT_CONTROL = unit_control.UnitControl(u)
        self.UNITS.add(u)

    def setUnit2(self,u):
        self.UNIT_2 = u
        self.UNIT_2_STATS = unit_stats.UnitStats(self.UNIT_2)
        self.UNIT_2.setPosition(self.GRID.getRandomGrassPosition())
        self.AI_CONTROL = unit_control.UnitControl(u)
        self.UNITS.add(u)
        self.LEARNING_UNIT = self.UNIT_2

    def addControl(self, uc):
        self.CONTROLS.append(uc)

