import numpy as np

#XOR
data = np.array([[0,0,0],[0,1,0],[1,0,0],[1,1,1]]);

wih1 = 0.3
wih2 = 0.2
wih3 = 0.8
wih4 = 0.1
wih5 = 0.2
wih6 = 0.8
wih7 = 0.4
wih8 = 0.7
bh1 = 0.4
bh2 = 0.2
bh3 = 0.6
bh4 = 0.3
who1 = 0.7
who2 = 0.2
who3 = 0.3
who4 = 0.2
bo = 0.6

lr = 0.001

Etotal = 100



w1 = np.array([[wih1,wih2],[wih3,wih4],[wih5,wih6],[wih7,wih8]])
w2 = np.array([[who1],[who2],[who3],[who4]])
b1 = np.array([[bh1],[bh2],[bh3],[bh4]])
b2 = np.array([[bo]])

k=0
# non-matrix result = Error is: [ 0.11268576]
while k <= 1000:
    k+=1
    for row in data:
        i1,i2 = row[0:2]
        target = row[2:3]
        
        ### FEED FORWARD
        neth1 = wih1*i1 + wih2*i2 + bh1
        neth2 = wih3*i1 + wih4*i2 + bh2
        neth3 = wih5*i1 + wih6*i2 + bh3
        neth4 = wih7*i1 + wih7*i2 + bh4
        i = np.array([[i1,i2]])

        #Shape 1x4
        neth = w1.dot(i.T) + b1
        outh = neth

        outh1 = neth1
        outh2 = neth2
        outh3 = neth3
        outh4 = neth4

        neto = outh1*who1 + outh2*who2 + outh3*who3 + outh4*who4 + bo
        neto_matrix = outh.T.dot(w2) + b2

        outo = neto
        outo_matrix = neto_matrix

        ## NE SE POLZVA tr se umnoji po slope-a
        Etotal = 1/2*(target-outo_matrix[0][0])**2

        ### BACKPROP
        ## dEtotal/douto * douto/dneto
        delta_o = - target + outo_matrix[0][0]
        
        ## dEtotal/dwho1
        dwho1 = delta_o * outh1
        ## dEtotal/dwho2
        dwho2 = delta_o * outh2
        ## dEtotal/dwho3
        dwho3 = delta_o * outh3
        ## dEtotal/dwho4
        dwho4 = delta_o * outh4

        dwho = np.array([delta_o]).dot(outh.T)

        ## delta_o * dneto/douth1 * douth1/dneth1
        delta_h1 = delta_o * who1
        ## delta_o + dneto/douth2 * douth2/dneth2
        delta_h2 = delta_o * who2
        ## delta_o + dneto/douth3 * douth3/dneth3
        delta_h3 = delta_o * who3
        ## delta_o + dneto/douth4 * outh4/dneth4
        delta_h4 = delta_o * who4

        delta_h = np.array([delta_o]).dot(w2.T)

        ## delta_h1 * dneth1/dwih1
        dwih1 = delta_h1 * i1
        ## delta_h2 * dneth2/dwih2
        dwih2 = delta_h1 * i2
        ## delta_h1 * dneth1/dwih1
        dwih3 = delta_h2 * i1
        ## delta_h2 * dneth2/dwih4
        dwih4 = delta_h2 * i2
        ## delta_h3 * dneth3/dwih5
        dwih5 = delta_h3 * i1
        ## delta_h3 * dneth3/dwih6
        dwih6 = delta_h3 * i2
        ## delta_h4 * dneth4/dwih7
        dwih7 = delta_h4 * i1
        ## delta_h4 * dneth4/dwin8
        dwih8 = delta_h4 * i2

        dwih = delta_h.T * i

        if(k==4):
            print(delta_h,i)
            print(dwih,dwih1,dwih2,dwih3,dwih4,dwih5,dwih6,dwih7,dwih8)
            exit()
        dbo = delta_o

        dbh1 = delta_h1
        dbh2 = delta_h2
        dbh3 = delta_h3
        dbh4 = delta_h4

        wih1 -= lr*dwih1
        wih2 -= lr*dwih2
        wih3 -= lr*dwih3
        wih4 -= lr*dwih4
        wih5 -= lr*dwih5
        wih6 -= lr*dwih6
        wih7 -= lr*dwih7
        wih8 -= lr*dwih8

        w1 -= dwih
        w2 -= dwho.T
        b1 -= delta_h.T

        bh1 -= lr*dbh1
        bh2 -= lr*dbh2
        bh3 -= lr*dbh3
        bh4 -= lr*dbh4
        who1 -= lr*dwho1
        who2 -= lr*dwho2
        who3 -= lr*dwho3
        who4 -= lr*dwho4
        bo -= lr*dbo

print("Error is: {}".format(Etotal))
