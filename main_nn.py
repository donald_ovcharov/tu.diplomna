import game_engine
import draw_engine
import unit
import unit_control
import pygame
import numpy as np
import random
import nn
from bot import Bot
from helpers import var_dump
from sklearn.neural_network import MLPRegressor

if __name__ == '__main__':
    env = game_engine.GameEngine('Node')
    ren = draw_engine.DrawEngine(env,'Node')

    
    env.restart()

# EXAMPLE NO AI
#    env.run()
#    exit();

    #Initialize table with all zeros
    #Example 1
#    n_state_space = 5
    #Example 2
#    n_state_space = 20
#    n_action_space = 5
#    Q = np.zeros([n_state_space,n_action_space])
#    Q_explored = np.zeros([n_state_space,n_action_space])
    # Set learning parameters
    lr = 2.
    y = .2
    eps = 0.2

    s,r = env.step(pygame.event.get())
    env.CLOCK.tick(env.FPS)

    action_map = [8,4,2,1]

    NN = nn.NeuralNetworkFromScratch()
    train = 300000
    #EXAMPLE 1
    #100000 - train to avoid lava but goes to lava again
    #EXAMPLE 2
    #1000000 - train to avoid lava + stays on grass
    i = 0
    consistancy = 1

    mem_x = np.array([])
    mem_y = np.array([])
    mem_a = np.array([])
    mem_counter = 0
    cost_counter = 0
    
    bot = Bot(env,env.UNIT_1)
    
    while True: #not env.checkWin():
        if(env.checkWin()):
            env.restart()
            bot = Bot(env,env.UNIT_1)
        i += 1
        if(i%10000 == 0):
            var_dump(i,"I")
            var_dump(NN.costs[len(NN.costs)-1],"Cost")
#        if(i%200000 == 0):
#            NN.plotSave("plots/image-"+str(i)+".png")
#            NN.costs = np.array([])
        #Choose an action by greedily (with noise) picking from Q table
        a_predicted = NN.predict(np.array(s))
        max_reward_action_index = np.argmax(a_predicted)
#        print(a_predicted,max_reward_action_index)

        if(consistancy == 1):
            if(random.uniform(0, 1) < eps):
    #            print("EPS > RAND")
                a = max_reward_action_index
            else:
    #            print("EPS < RAND")
    #@TODO No longer keeping track of explored..
    # So just pick random
    #                if(random.uniform(0,1) < 0.5):
    #                    a = np.argmin(Q_explored[s,:])
    #                else:
    #                    a = random.choice([0,1,2,3,4])
                a = random.choice([0,1,2,3])
                consistancy = 5
        else:
            a = a
            consistancy -= 1
        #else:
        #   a = random from Q
#        if(i <= train):
#            a = action_map_reverse[env.LEARNING_UNIT.action_taken]
        #Get new state and reward from environment
#        print("Taken action is:",a)

        env.AI_CONTROL.dispatchAction(action_map[a])
        env.BOT_CONTROL.dispatchAction(bot.getAction())
        bot.shoot()
#        if(env.GRID.AMMOS[0].rect.center)
        
        s1,r = env.step(pygame.event.get())
#        print("Reward is:",r)
        #if(not (s1==s).all()):
         #   consistancy = 0
            #Update Q-Table with new knowledge
        if(consistancy == 5):
                #Q[s,a] = Q[s,a] + lr*(r + y*np.max(Q[s1,:]) - Q[s,a])
            a_actual = np.copy(a_predicted[0])
#            a_actual = np.zeros(4)#np.copy(a_predicted)#np.zeros(5)
#            a_actual = np.zeros(4)
            a1_predicted = NN.predict(np.array(s1))
            max_future = np.argmax(a1_predicted)
            a_actual[a] = r + y*max_future
            mem_x = np.append(mem_x,[s])
            mem_y = np.append(mem_y,[a_actual])
            mem_counter += 1
            if(mem_counter >= 1):
                X = mem_x.reshape(-1,NN.input_count)
                Y = mem_y.reshape(-1,NN.output_count)
                rec_cost = True if cost_counter == 5 else False
                cost_counter = cost_counter + 1 if cost_counter < 5 else 0
                NN.fit_single(X,Y,record_cost=rec_cost)
                mem_x=np.array([])
                mem_y=np.array([])
                mem_a=np.array([])
                mem_counter = 0
            
        s = s1
        if(i == train*1/4):
            eps = 0.3
#            NN.lr *= 0.1
        if(i == train*1/2):
#           pass
#            y=0.7
#            eps = 0.6
            NN.lr *= 0.5
        if(i == train*3/4):
#            pass
            NN.lr *= 0.5
        if(i == train*7/8):
#            pass
            NN.lr *= 0.1
        if(i == train):
            eps = 1
            NN.plot()
        if(i > train):
            #EXAMPLE SHOW HEAT MAP
#            env.showTrainedMap(Q)
            #NN.plot()
            #exit()
#            env.LEARNING_UNIT = env.UNIT_1
#            var_dump(a_predicted,"A-Predicted")
#            var_dump(np.argmax(a_predicted),"Arg-Max")
            env.CLOCK.tick(env.FPS)
#            print(s,a_predicted)
            ren.render()
#            while True:
#                pass
#        env.CLOCK.tick(env.FPS)
#        ren.render()
