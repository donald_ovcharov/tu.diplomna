class AppliedForce:

    GRAVITY = 9.8

    def __init__(self, N, target_mass, angle):
        #@TODO accept Newtons, and calculate acceleration for a given mass
        self.N = N
        self.target_mass = target_mass
        self.acceleration = N/target_mass
        self.velocity = self.acceleration/20
        self.angle = angle
        self.applied = False

class Friction(AppliedForce):
    def __init__(self, ms, mk):
        self.ms = ms
        self.mk = mk
