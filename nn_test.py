import numpy as np
np.random.seed(1)
#Input array
X=np.array([[1,0,1,0],[1,0,1,1],[0,1,0,1]])

#Output
y=np.array([[1],[1],[0]])

#Sigmoid Function
def sigmoid (x):
    return 1/(1 + np.exp(-x))

#Derivative of Sigmoid Function
def derivatives_sigmoid(x):
    return x * (1 - x)

#Variable initialization
epoch=5000 #Setting training iterations
lr=0.1 #Setting learning rate
inputlayer_neurons = X.shape[1] #number of features in data set
hiddenlayer_neurons = 3 #number of hidden layers neurons
output_neurons = 1 #number of neurons at output layer

#weight and bias initialization
#w1=np.random.uniform(size=(inputlayer_neurons,hiddenlayer_neurons))
#b1=np.random.uniform(size=(1,hiddenlayer_neurons))
#w2=np.random.uniform(size=(hiddenlayer_neurons,output_neurons))
#b2=np.random.uniform(size=(1,output_neurons))
w1 = 2*np.random.random((inputlayer_neurons,hiddenlayer_neurons)) - 1
w2 = 2*np.random.random((hiddenlayer_neurons,output_neurons)) - 1
b1 = 2*np.random.random((1,hiddenlayer_neurons)) - 1
b2 = 2*np.random.random((1,output_neurons)) - 1

for i in range(epoch):

    #Forward Propogation

    z1=np.dot(X,w1) + b1
    a1 = sigmoid(z1)
    z2= np.dot(a1,w2) + b2
    a2 = sigmoid(z2)

    #Backpropagation
    E = y-a2
    slope_a2 = derivatives_sigmoid(a2)
    slope_a1 = derivatives_sigmoid(a1)
    deltaL3 = E * slope_a2
    deltaL2 = np.dot(deltaL3,w2.T) * slope_a1

    deltaW2 = np.dot(a1.T,deltaL3)
    deltaW1 = np.dot(X.T,deltaL2)     

    deltaB2 = np.sum(deltaL3, axis=0,keepdims=True)
    deltaB1 = np.sum(deltaL2, axis=0,keepdims=True)
    
    w2 += deltaW2 *lr
    b2 += deltaB2 *lr
    w1 += deltaW1 *lr
    b1 += deltaB1 *lr

print(a2)
