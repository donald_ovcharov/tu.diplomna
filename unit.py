import pygame
import force
import math
import helpers

class Unit(pygame.sprite.Sprite):
    
    WIDTH = 20
    HEIGHT= 20
    COLOR = pygame.Color(0,0,0)

    def __init__(self, mass, image,env):
        self.env = env
        super().__init__()

        self.image = image
        self.rect = self.image.get_rect()

        self.Forces = []
        self.BULLETS = pygame.sprite.Group()
        self.LOADED_BULLET = None
        self.reload()

        #Physics
        self.mass = mass
        self.acceleration = 0.0
        self.angle = 0.0
        self.velocity = 0.0

        self.stopFrictionForce()
        self.setPosition((0,0))

        self.upCtrl = 0
        self.downCtrl = 0
        self.rightCtrl = 0
        self.leftCtrl = 0
        self.action_taken = 0

        self.isFlying = False

        self.health = 100
        self.hit_factor = 1

    def processFrame(self, moment):
        self.processPosition(moment)
        self.processDMG(moment)
        self.LOADED_BULLET.moment(moment)

    def processPosition(self, moment):
        for i,f in enumerate(self.Forces):
                self.handleForce(f)
                del self.Forces[i]

        self.velocity += + self.acceleration*moment

        if(not self.isFlying):
            self.velocity -= self.friction_force.acceleration*moment

        self.move(moment)

        if(self.velocity > 0):
            self.startFrictionForce()
        else:
            self.velocity = 0
            self.stopFrictionForce()

    def processDMG(self,moment):
        if(not self.isFlying):
            self.health -= self.field.dps * moment

## VECTOR SUM
    def handleForce(self,force):
#        print ("self - velocity: {}, angle {}".format(self.velocity,self.angle))
#        print ("force - velocity: {}, angle {}".format(force.velocity,force.angle))

        cur_magnitude = self.velocity
        cur_angle = self.angle

        f_magnitude = force.velocity
        f_angle = force.angle

        cur_x = cur_magnitude * math.cos(math.radians(cur_angle))
        cur_y = cur_magnitude * math.sin(math.radians(cur_angle))

        f_x = f_magnitude * math.cos(math.radians(f_angle))
        f_y = f_magnitude * math.sin(math.radians(f_angle))

        sum_x = cur_x + f_x
        sum_y = cur_y + f_y

        sum_magnitude = math.sqrt(sum_x**2+sum_y**2)

        sum_angle = math.degrees(math.atan2(sum_y,sum_x))
        self.angle = sum_angle
        self.velocity = sum_magnitude
#        print ("resultant - velocity: {}, angle {}".format(sum_magnitude,sum_angle))
        return sum_magnitude - cur_magnitude

    def handleHits(self, hits):
        for hit in hits:
            self.handleHit(hit)

    def handleHit(self, hit):
        self.applyForce(hit.mass*4000*self.hit_factor,helpers.getAngle(hit.rect.center,self.rect.center))
        self.hit_factor += 0.2

    def shoot(self, mouse_pos):
        bullet = self.LOADED_BULLET.shoot(self.rect.center,mouse_pos)
        if(bullet):
            self.BULLETS.add(bullet)
            self.reload()

    def reload(self):
        self.LOADED_BULLET = LoadedBullet(self.env)

    def setPosition(self, position):
        self.float_x, self.float_y = position

    def move(self, moment):
        #Physics Movement
        self.float_y += -1*math.sin(math.radians(self.angle))*self.velocity*moment
        self.float_x += math.cos(math.radians(self.angle))*self.velocity*moment

        #Control Movement
        self.moveCtrl(self.upCtrl,self.downCtrl,self.leftCtrl,self.rightCtrl)
    
        if(not self.isFlying):
            w_margin = self.rect.width/2
            h_margin = self.rect.height/2
            if(self.float_x > self.env.WIDTH - w_margin):
                self.float_x = self.env.WIDTH - w_margin
            if(self.float_x < w_margin):
                self.float_x = w_margin
            if(self.float_y > self.env.HEIGHT - h_margin):
                self.float_y = self.env.HEIGHT - h_margin
            if(self.float_y < h_margin):
                self.float_y = h_margin
        
        self.rect.center = (self.float_x, self.float_y)

    def moveCtrl(self,u,d,l,r):
        self.action_taken = 0
        if(u):
            self.action_taken = 8
        elif(d):
            self.action_taken = 4
        elif(l):
            self.action_taken = 2
        elif(r):
            self.action_taken = 1

        speed = 200
        self.float_x = self.float_x + r*speed*self.env.MOMENT - l*speed*self.env.MOMENT
        self.float_y = self.float_y + d*speed*self.env.MOMENT - u*speed*self.env.MOMENT


    def startFrictionForce(self):
        self.friction = force.Friction(0.3,5.0)
        self.friction_force = force.AppliedForce(self.field.friction_mk*force.AppliedForce.GRAVITY*self.mass,self.mass, 270)

    def stopFrictionForce(self):
        self.friction = force.Friction(0.3,0.3)
        self.friction_force = force.AppliedForce(0,self.mass, 270)

    def applyForce(self, N, angle):
        self.Forces.append(force.AppliedForce(N, self.mass, angle))

class LoadedBullet:
    COOLDOWN = 0.6
    def __init__(self, env, mass = 5):
        self.env = env
        self.mass = mass
        self.time = 0

    def moment(self, moment):
        self.time += 1*moment

    def collect(self, ammos):
        for ammo in ammos:        
            self.mass += ammo.mass

    def shoot(self, gun_pos, mouse_pos):
        if(self.time > self.COOLDOWN):
            b = Bullet(gun_pos,self.mass,helpers.getAngle(gun_pos,mouse_pos),self.env)
            return b

class Bullet(Unit):
    def __init__(self, position, mass, angle,env):
#        i = pygame.Surface([5, 5])
#        i.fill(pygame.Color(128, 0, 0,255))
        i = pygame.image.load("assets/crawl-tiles/item/misc/misc_crystal_cropped.png")
        super().__init__(mass,i,env)
        self.setPosition(position)
        self.angle = angle
        self.velocity = 300
        self.isFlying = True
