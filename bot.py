from helpers import var_dump
from unit_control import UnitControl
import numpy as np
class Bot:
    def __init__(self,env,unit):
        self.env = env
        self.unit = unit
    
    def getAction(self):
        ammo_rel_pos = np.subtract(self.env.GRID.AMMOS.sprites()[0].rect.center,self.unit.rect.center)
        if(np.abs(ammo_rel_pos[0]) > np.abs(ammo_rel_pos[1])):
            #left-right
            if(ammo_rel_pos[0] > 0):
                return UnitControl.AI_ACTION_RIGHT
            else:
                return UnitControl.AI_ACTION_LEFT
        else:
            #up-down
            if(ammo_rel_pos[1] > 0):
                return UnitControl.AI_ACTION_DOWN
            else:
                return UnitControl.AI_ACTION_UP

    def shoot(self):
        self.unit.shoot(self.env.UNIT_2.rect.center)
