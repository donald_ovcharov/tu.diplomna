# README #

Oponent based game for custom environment. The game is simple:

Get swords to have a stronger bullet (bullet factor).

Shoot at opponent and try to push him on the lava.

Being on a lava field damages your health per second.

Ice fields are more slippery.

Being hit makes you more vulnerable to follow up hits (hit factor)

The environment follows AI gym api standard (env.reset, env.step, env.render etc)

Initially I have implemented it with Q-table but wasn't sufficient due to big state space, so I have decided to go for NN to replace the Q-Table. This is called DQN and has an awesome paper from DeepMind (source bellow).

NN from scratch ! is used for Q approximation.

Model(Policy) free.

Hyperparameters are hand tuned as well.

Inputs are hand picked features but I have purposfully made them non-straight forward for e.g. I am feeding the NN with (X,Y) values instead of distances so that the NN has to understand the concept of map positions.

Some of the input values for the NN are:

 - x and y values of player 1
 
 - x and y values of player 2
 
 - x and y values of player 1 bullets
 
 - x and y values of sword
 
 - health and cooldown of player 1
 
 - health and cooldown of player 2
 
 - hit factor and bullet factor of player 1
 
 - hit factor and bullet factor of player 2
 
 - others..
 


My success with the AI is that it learns to avoid bullets. Can't shoot yet. 

Successfully implemented DQN algorithm: https://deepmind.com/research/dqn/

Option to play vs human when AI is learned (must be enabled through code)

Video of the learned AI: https://drive.google.com/open?id=0B006o88SPC9UTFowMEJYcmFGMHc


### How do I get set up? ###

source .env/bin/activate

python main_nn.py


### What happens ###
At first the AI is training against a hard policy bot

At 10k steps error value is outputed in console.

At 200k steps error graph is displayed.

When graph is closed game is rendered (AI vs bot). You can modify the code to swith the AI control to human and you can test your skills against the AI :)


### Contacts ###
donaldovcharov@gmail.com