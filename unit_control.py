import pygame
import helpers

class UnitControl:

    def __init__(self, unit):
        self.unit = unit

    def dispatch(self, event):
        if event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
            self.unit.downCtrl = 1
        elif event.type == pygame.KEYUP and event.key == pygame.K_DOWN:
            self.unit.downCtrl = 0
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_UP:
            self.unit.upCtrl = 1
        elif event.type == pygame.KEYUP and event.key == pygame.K_UP:
            self.unit.upCtrl = 0
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
            self.unit.leftCtrl = 1
        elif event.type == pygame.KEYUP and event.key == pygame.K_LEFT:
            self.unit.leftCtrl = 0
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
            self.unit.rightCtrl = 1
        elif event.type == pygame.KEYUP and event.key == pygame.K_RIGHT:
            self.unit.rightCtrl = 0
        elif event.type == pygame.MOUSEBUTTONDOWN:
            self.unit.shoot(pygame.mouse.get_pos())

    AI_ACTION_UP = 8
    AI_ACTION_DOWN = 4
    AI_ACTION_LEFT = 2
    AI_ACTION_RIGHT = 1

    def dispatchAction(self, action):
        self.unit.moveCtrl(*helpers.bitfield(action))

    def dispatchUp(self):
        self.unit.moveCtrl(*helpers.bitfield(AI_ACTION_UP))
    def dispatchDown(self):
        self.unit.moveCtrl(*helpers.bitfield(AI_ACTION_DOWN))
    def dispatchLeft(self):
        self.unit.moveCtrl(*helpers.bitfield(AI_ACTION_LEFT))
    def dispatchRight(self):
        self.unit.moveCtrl(*helpers.bitfield(AI_ACTION_RIGHT))

    def getActionSpace(self):
        action_space = [
            AI_ACTION_DOWN,
            AI_ACTION_UP,
            AI_ACTION_LEFT,
            AI_ACTION_RIGHT
        ]
        return action_space
