import numpy as np
from helpers import var_dump
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.special import expit

np.seterr(all='raise')



class NeuralNetworkFromScratch:
    
    costs = np.array([])
    
    def __init__(self):
        #Important for debug, random gets deterministic !
        np.random.seed(1)
        self.input_count = 26
        self.hidden_count = 10
        self.hidden_count2 = 6
        self.hidden_count3 = 6
        self.output_count = 4
        self.wx1 = 2*np.random.random((self.input_count,self.hidden_count)) - 1
        self.w12 = 2*np.random.random((self.hidden_count,self.hidden_count2)) - 1
        self.w23 = 2*np.random.random((self.hidden_count2,self.hidden_count3)) - 1
        self.w3y = 2*np.random.random((self.hidden_count3,self.output_count)) - 1
        self.bx1 = 2*np.random.random((1,self.hidden_count)) - 1
        self.b12 = 2*np.random.random((1,self.hidden_count2)) - 1
        self.b23 = 2*np.random.random((1,self.hidden_count3)) - 1
        self.b3y = 2*np.random.random((1,self.output_count)) - 1

        #when working with ReLU
#        self.wx1 = np.abs(self.wx1)
#        self.w12 = np.abs(self.w12)
#        self.w23 = np.abs(self.w23)
#        self.w3y = np.abs(self.w3y)
#        self.bx1 = np.abs(self.bx1)
#        self.b12 = np.abs(self.b12)
#        self.b23 = np.abs(self.b23)
#        self.b3y = np.abs(self.b3y)
        self.lr = 1e-4
        self.costs = np.array([0])

    def fit(self,X,Y):
        for i in np.arange(100):
#            if(i%100==0):
#                var_dump(i,"Iteration")
#                var_dump(self.costs[len(self.costs)-1],"Error")
#            for row , target in zip(X,Y):
#                self.fit_single(row.reshape(1,self.input_count),target.reshape(1,self.output_count))
#            for i in np.arange(100):
#                batch_size_coef = 1/100
#                batch_size = int(np.round(len(X)*batch_size_coef))
#                offset = int(i*batch_size)
#                self.fit_single(X[offset:offset+batch_size],Y[offset:offset+batch_size])
            self.fit_single(X,Y,True)


    def fit_single(self,x,target,record_cost=False):

        z0,a0,z1,a1,z2,a2,z3,a3 = self.predict(x,True)
#        var_dump(target,"target")
#        var_dump(a2,"a2")
        cost = 0.5*np.sum(((target - a3)**2))

        E = target-a3
        slope_a3 = self.tanh_deriv(a3)
        slope_a2 = self.tanh_deriv(a2)
        slope_a1 = self.tanh_deriv(a1)
        slope_a0 = self.tanh_deriv(a0)

        deltaLY = E * slope_a3
        deltaL3 = np.dot(deltaLY,self.w3y.T) * slope_a2
        deltaL2 = np.dot(deltaL3,self.w23.T) * slope_a1
        deltaL1 = np.dot(deltaL2,self.w12.T) * slope_a0

        deltaW3 = np.dot(a2.T,deltaLY)
        deltaW2 = np.dot(a1.T,deltaL3)
        deltaW1 = np.dot(a0.T,deltaL2)
        deltaW0 = np.dot(x.T,deltaL1)

        deltaB3 = np.sum(deltaLY, axis=0,keepdims=True)
        deltaB2 = np.sum(deltaL3, axis=0,keepdims=True)
        deltaB1 = np.sum(deltaL2, axis=0,keepdims=True)
        deltaB0 = np.sum(deltaL1, axis=0,keepdims=True)
#        var_dump(deltaW3*self.lr/self.w3y,"deltaW3/w3y")
#        var_dump(deltaW2*self.lr/self.w23,"deltaW2/w23")
#        var_dump(deltaW1*self.lr/self.w12,"deltaW1/w12")
#        var_dump(deltaW0*self.lr/self.wx1,"deltaW0/wx1")
#        var_dump(self.wx1,"w0")
#        var_dump(self.w12,"w1")
#        var_dump(self.w23,"w2")
#        var_dump(self.bx1,"b0")
#        var_dump(self.b12,"b1")
#        var_dump(self.b23,"b2")

        self.wx1 += deltaW0*self.lr
        self.w12 += deltaW1*self.lr*10
        self.w23 += deltaW2*self.lr*100
        self.w3y += deltaW3*self.lr*1000

        self.b3y += deltaB3*self.lr*1000
        self.b23 += deltaB2*self.lr*100
        self.b12 += deltaB1*self.lr*10
        self.bx1 += deltaB0*self.lr
        if(record_cost):
            self.costs = np.append(self.costs,cost)
#            print("Error is: ",cost)


    #Sigmoid Function
    def sigmoid (self,x):
#        x = np.clip(x, -1.e+2, 1.e+2)
        return 1/(1 + np.exp(-x))

    #Derivative of Sigmoid Function
    def derivatives_sigmoid(self,x):
#        x = np.clip(x, -1.e+10, 1.e+10)
        return x * (1 - x)

    def tanh(self,x):
        return np.tanh(x)

    def tanh_deriv(self,x):
        return 1.0 - np.tanh(x)**2

    def relu(self,x):
        return x * (x > 0)

    def relu_deriv(self,x):
        return 1. * (x > 0)

    def predict(self,x,use_for_fit=False):
##CONFIRMED
    
        z0 = np.dot(x,self.wx1) + self.bx1
        a0 = self.tanh(z0)        

        z1 = np.dot(a0,self.w12) + self.b12
        a1 = self.tanh(z1)

        z2 = np.dot(a1,self.w23) + self.b23
        a2 = self.tanh(z2)
    
        z3 = np.dot(a2,self.w3y) + self.b3y
        a3 = self.tanh(z3)
        
        if(use_for_fit):
            return [z0,a0,z1,a1,z2,a2,z3,a3]
        else:
            return a3

    def plot(self):
        plt.plot(self.costs)
        plt.show()
    def plotSave(self,name):
        plt.plot(self.costs)
        plt.savefig(name)
        plt.clf()
        

## input dataset
#X = np.arange(0,0.9,0.01).reshape(2,-1)
#Y = np.sqrt(X[0]**2 + X[1]**2).reshape(1,-1)
#X = X.T
#Y = Y.T

#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')
#ax.plot(X[0],X[1],Y)
#plt.show()

#NN = NeuralNetworkFromScratch()
#NN.fit(X,Y)

#print(NN.costs)
#plt.plot(NN.costs)
#plt.show()
'''
X = np.arange(-20.,20.,0.1)
np.random.shuffle(X)
X = X.reshape(-1,1)
Y = np.tanh(X/2)*0.7

NN = NeuralNetworkFromScratch()
NN.fit(X,Y)
NN.plot()

pred = NN.predict(X)

plt.plot(X.flatten(),Y.flatten(),'r+')
plt.plot(X.flatten(),pred.flatten(),'b+')
plt.show()
var_dump(NN.w0,"W0")
var_dump(NN.w1,"W1")
var_dump(NN.w2,"W2")
'''
