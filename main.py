import game_engine
import draw_engine
import unit
import unit_control
import pygame
import numpy as np
import random

if __name__ == '__main__':
    env = game_engine.GameEngine('Node')
    ren = draw_engine.DrawEngine(env,'Node')

    HUMAN_UNIT_IMAGE = pygame.image.load("assets/images/mk3_liu_kang.gif")
    HUMAN_UNIT = unit.Unit(10, HUMAN_UNIT_IMAGE)
    HUMAN_UNIT.rect.x = 100

    AI_UNIT_IMAGE = pygame.image.load("assets/images/mk3_liu_kang.gif")
    AI_UNIT = unit.Unit(10,AI_UNIT_IMAGE)

    UC = unit_control.UnitControl(HUMAN_UNIT)

    env.setUnit1(HUMAN_UNIT)
    env.setUnit2(AI_UNIT)
    env.addControl(UC)

# EXAMPLE NO AI
#    env.run()
#    exit();

    #Initialize table with all zeros
    #Example 1
#    n_state_space = 5
    #Example 2
    n_state_space = 20
    n_action_space = 5
    Q = np.zeros([n_state_space,n_action_space])
    Q_explored = np.zeros([n_state_space,n_action_space])
    # Set learning parameters
    lr = .2
    y = .4
    eps = 0.2

    s,r = env.step(pygame.event.get())
    env.CLOCK.tick(env.FPS)

    action_map = [8,4,2,1,0]

    train = 1000000
    #EXAMPLE 1
    #100000 - train to avoid lava but goes to lava again
    #EXAMPLE 2
    #1000000 - train to avoid lava + stays on grass
    i = 0
    consistancy = 0
    while True: #not env.checkWin():
        i += 1
        #Choose an action by greedily (with noise) picking from Q table
        if(consistancy == 0):
            if(random.uniform(0, 1) < eps):
                a = np.argmax(Q[s,:])
            else:
                if(random.uniform(0,1) < 0.5):
                    a = np.argmin(Q_explored[s,:])
                else:
                    a = random.choice([0,1,2,3,4])
            consistancy = 10
        else:
            a = a
            consistancy -= 1
        #else:
        #   a = random from Q
        
        #Get new state and reward from environment
        env.AI_CONTROL.dispatchAction(action_map[a])
        Q_explored[s,a] += 0.2
        s1,r = env.step(pygame.event.get())
        
        if(s1 != s):
            consistancy = 0
            #Update Q-Table with new knowledge
            if(i <= train):
                Q[s,a] = Q[s,a] + lr*(r + y*np.max(Q[s1,:]) - Q[s,a])
        s = s1
        if(i % 1000 == 0):
            print("I = {}".format(i))

        if(i % 100000 == 0):
            print("##################################################################")
            print(Q)

        if(i == train*1/2):
            eps = 0.3
        if(i == train*3/4):
            eps = 0.4
        if(i == train):
            eps = 1
        if(i > train):
            #EXAMPLE SHOW HEAT MAP
#            env.showTrainedMap(Q)
            if(i == train + 1):
                print("##################################################################")
                print(Q)
            ren.render()
#            while True:
#                pass
#        env.CLOCK.tick(env.FPS)
#        ren.render()
